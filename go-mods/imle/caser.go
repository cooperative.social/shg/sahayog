package imle

import (
	"time"
)

//Caser CAS Storage Provider
type Caser interface {
	Store(data DataHasher,  ns string) (cas CasAddress, err error)
	Load(cas CasAddress) (data DataHasher,  ns string, err error)
}

//NamedCasStore much like IPNS or DNS, maps Name to a CAS Address
type NamedCasStore interface {
	HasCas(id string) ( bool, error)
	GetCas(id string) ( cas CasAddress,tmstamp time.Time, err error)
    PutCas(id string, cas CasAddress) (tmstamp time.Time, err error)
}