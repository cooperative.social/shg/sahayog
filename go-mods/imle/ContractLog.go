package imle

import (
	"crypto/sha512"
	"merkle"
	"time"
)

//MerkleDataLog Merkle Data of a Log.
// enables for role based selective data element access
// partial data can be verified against merkle proof
type MerkleDataLog struct {
	Meta       []Trie    `json:"meta"`
	MerkleRoot []byte    `json:"merkleRoot"`
	Data       []Trie    `json:"data"`
	Timestamp  time.Time `json:"tmstamp"`
}

//MerkleProof generates the merkle proof of the Data Log
func (md *MerkleDataLog) MerkleProof() (rootHash []byte, proofs map[string]*merkle.SimpleProof, keys []string) {
	m := make(map[string]merkle.Hasher)
	for _, ele := range md.Data {
		m[ele.Key.String()] = ele.Value
	}
	return merkle.SimpleProofsFromMap(m)
}

// Trie TLV (Type Label Value) for merkle Hash,
// key = namespace : label
// value isused to compute the Hash
type Trie struct {
	Key   Key        `json:"key"`
	Value DataHasher `json:"value"`
}

//Key namespaced label
type Key struct {
	Namespace string `json:"namespace"`
	Label     string `json:"label"`
}

func (k Key) String() string {
	return k.Namespace + ":" + k.Label
}

//CasAddress CAS address
type CasAddress []byte

//Hash implements merkle.Hasher
func (h CasAddress) Hash() []byte {
	return ([]byte(h))
}

//DataHasher data that partakes in Merkle Hashing
type DataHasher []byte

//Hash implements merkle.Hasher
func (h DataHasher) Hash() []byte {
	return Hash([]byte(h))
}

// Hash returns SHA-2 SHA-512 hash
func Hash(in []byte) []byte {
	hash := sha512.New()
	hash.Write(in)
	return hash.Sum(nil)
}
