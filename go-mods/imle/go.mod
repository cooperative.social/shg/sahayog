module imle

go 1.13

require (
	merkle v0.0.0
	qz v0.0.0

)

replace (
	merkle => ../merkle
	qz => ../qz

)
