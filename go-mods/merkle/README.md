## Simple Merkle Tree

For smaller static data structures that don't require immutable snapshots or mutability; 
for instance the transactions and validation signatures of a block can be hashed using this simple merkle tree logic.

Extracted from original code [tendermint merkle](https://github.com/tendermint/tendermint/tree/master/crypto/merkle) 

