module merkle

go 1.13

require (
	github.com/go-kit/kit v0.10.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/tendermint/go-amino v0.15.1
	github.com/tendermint/go-crypto v0.9.0
	github.com/tendermint/tmlibs v0.9.0
)
