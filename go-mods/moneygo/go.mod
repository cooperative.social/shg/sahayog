module moneygo

go 1.13

require (
	github.com/aclindsa/gorp v2.0.0+incompatible
	github.com/aclindsa/moneygo v0.0.0-20190113012720-285b6322392d
	github.com/aclindsa/ofxgo v0.1.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/kabukky/httpscerts v0.0.0-20150320125433-617593d7dcb3
	github.com/lib/pq v1.5.2
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
